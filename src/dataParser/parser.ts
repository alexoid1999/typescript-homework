export const key = "a87aea83c0e700c146fe55e9860a4f45";


export const getFieldsLikeInType = <T, K extends keyof T>(base: T, keys: K[]): Pick<T,K> => {
    const entries = keys.map(key=> ([key, base[key]]));
    return Object.fromEntries(entries)
};

const http = async<T> (request:RequestInfo): Promise<T> => {
    const response = await fetch("https://api.themoviedb.org/3"+request);
    const body = await response.json();
    return body
};

export default http;