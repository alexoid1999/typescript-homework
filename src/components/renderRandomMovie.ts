import { Movie } from "../types/movieType";


export const renderRandomMovie = (moviesListRef: HTMLElement | null, movie: Movie): void => {
    const img = document.createElement("img");
    img.src = `https://image.tmdb.org/t/p/original/${movie.backdrop_path}`;
    img.setAttribute("style", "padding: 0;");

    const overview = document.createElement("p");
    overview.className = "card-text";
    overview.innerText = movie.overview;

    const title = document.createElement("h1");
    title.className = "fw-light text-light";
    title.innerText = movie.title;

    const description = document.createElement("h1");
    description.className = "lead text-white";
    description.innerText = movie.overview;

    const card = document.createElement("div");
    card.className = "col-lg-6 col-md-8 mx-auto";
    card.getAttribute("width")
    card.setAttribute("style", "background-color: #2525254f; position: absolute;");


    card.append(title);
    card.append(description);

    const movieContainer = document.createElement("div");
    movieContainer.className = "row py-lg-5";
    movieContainer.setAttribute("style", "position: relative; justify-content: center; align-items: center;")
    movieContainer.append(img);
    movieContainer.appendChild(card);

    moviesListRef && moviesListRef.append(movieContainer);

}