import { LocalStorageWorker } from "../services/storageService";
import { renderMovie } from "./renderMovie";

const localstorage = new LocalStorageWorker();

const renderFavoriteList = async (moviesListRef: HTMLElement | null): Promise<void> => {
    const moviesList = await localstorage.getAllItems();

    moviesList.forEach(movie => {
        renderMovie(moviesListRef, movie.value, true);
    })
}

export default renderFavoriteList;