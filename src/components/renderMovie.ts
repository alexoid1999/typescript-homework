import { LocalStorageWorker } from "../services/storageService";
import { Movie } from "../types/movieType";

const localstorage = new LocalStorageWorker();

export const renderMovie = (moviesListRef: HTMLElement | null, movie: Movie, favoriteList: boolean): void => {
    let savedMovie = localstorage.get(movie.id.toString());
    const img = document.createElement("img");
    img.src = `https://image.tmdb.org/t/p/original/${movie.poster_path}`;

    const svgBlock = document.createElementNS("http://www.w3.org/2000/svg","svg");
    svgBlock.setAttribute("class","bi bi-heart-fill position-absolute p-2 like");
    svgBlock.setAttribute("stroke","red");
    svgBlock.setAttribute("fill", !savedMovie ? "#ff000078" : "red");
    svgBlock.setAttribute("width","50");
    svgBlock.setAttribute("height","50");
    svgBlock.setAttribute("viewBox","0 -2 18 22");

    svgBlock.addEventListener("click", () => {
        if(!savedMovie) {
            svgBlock.setAttribute("fill", "red");
            localstorage.add(movie.id.toString(), movie);
        }
        else {
            svgBlock.setAttribute("fill", "#ff000078");
            localstorage.remove(movie.id.toString());
        }
        savedMovie = localstorage.get(movie.id.toString());
    });

    const path = document.createElementNS("http://www.w3.org/2000/svg", "path");
    path.setAttribute("fill-rule","evenodd");
    path.setAttribute("d","M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z");

    svgBlock.append(path);

    const overview = document.createElement("p");
    overview.className = "card-text truncate";
    overview.innerText = movie.overview;

    const release = document.createElement("div");
    release.className = "d-flex justify-content-between align-items-center";

    const releaseText = document.createElement("small");
    releaseText.className = "text-muted";
    releaseText.innerText = movie.release_date;

    release.append(releaseText);


    const cardBody = document.createElement("div");

    cardBody.append(overview);
    cardBody.append(release);

    const card = document.createElement("div");
    card.className = "card shadow-sm";

    card.append(img);
    card.append(svgBlock);
    card.append(cardBody);

    const movieContainer = document.createElement("div");
    movieContainer.className = "col-lg-3 col-md-4 col-12 p-2";
    movieContainer.appendChild(card);
    
    if(favoriteList) {
        moviesListRef && moviesListRef.append(card);
    }
    else {
        moviesListRef && moviesListRef.append(movieContainer);
    }
}