import getMovies from "../services/getMovies";
import { GetMovies } from "../types/serviceType";
import { renderMovie } from "./renderMovie";
import { renderRandomMovie } from "./renderRandomMovie";
import clearContainer from "../services/clearContainer";

const renderMoviesList = async (moviesListRef: HTMLElement | null, mode: GetMovies): Promise<void> => {
    const moviesList = await getMovies(mode);

    const randomMovieContainer = document.getElementById("random-movie");

    randomMovieContainer && clearContainer(randomMovieContainer);

    renderRandomMovie(
        randomMovieContainer, 
        moviesList.results[Math.floor(Math.random()*(moviesList.results.length))]
    )

    moviesList.results.forEach(movie => {
        renderMovie(moviesListRef, movie, false);
    })
}

export default renderMoviesList;