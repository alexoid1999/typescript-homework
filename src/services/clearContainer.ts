const clearContainer = (container: HTMLElement | Element): void => {
    while (container.firstChild && container.lastChild) {
        container.removeChild(container.lastChild);
    }
}

export default clearContainer