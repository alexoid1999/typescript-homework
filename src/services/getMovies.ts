import {Movie, nullMovie, PageMovies} from "../types/movieType";
import {GetMovies} from "../types/serviceType";
import http, {getFieldsLikeInType, key} from "../dataParser/parser";


const getMovies = async (mode: GetMovies) :Promise<PageMovies> => {
    const query = mode.by === "search" 
        ? `/search/movie?api_key=${key}&query=${mode.filmName}&page=${mode.page}`
        : `/movie/${mode.by}?api_key=${key}&page=${mode.page}`;

    const result = 
        await http<PageMovies>(query);
    return {
        ...result,
        results: result.results.map( movie => 
            getFieldsLikeInType(movie, Object.keys(nullMovie) as Array<keyof Movie>)
        )
    }
}

export default getMovies