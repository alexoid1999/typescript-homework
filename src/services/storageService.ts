import { IStorageItem } from "../types/localstorageType";
import { Movie } from "../types/movieType";

export class StorageItem {
    key: string;
    value: Movie;

    constructor(data: IStorageItem) {
        this.key = data.key;
        this.value = data.value;
    }
}

export class LocalStorageWorker {
    localStorageSupported: boolean;

    constructor() {
        this.localStorageSupported = typeof window['localStorage'] != "undefined" && window['localStorage'] != null;
    }

    add(key: string, item: Movie): void {
        if (this.localStorageSupported) {
            localStorage.setItem(key, JSON.stringify(item));
        }
    }

    getAllItems(): Array<StorageItem> {
        const list = new Array<StorageItem>();

        for (let i = 0; i < localStorage.length; i++) {
            const key = localStorage.key(i);
            const value = key && localStorage.getItem(key);

            key && value && list.push(new StorageItem({
                key,
                value: JSON.parse(value) as Movie
            }));
        }

        return list;
    }

    get(key: string): Movie | undefined {
        if (this.localStorageSupported) {
            const item = localStorage.getItem(key);
            return item ? JSON.parse(item) as Movie : undefined;
        } else {
            return undefined;
        }
    }

    remove(key: string): void {
        if (this.localStorageSupported) {
            localStorage.removeItem(key);
        }
    }
}