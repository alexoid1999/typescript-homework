import { Movie } from "./movieType";

export interface IStorageItem {
    key: string;
    value: Movie;
}