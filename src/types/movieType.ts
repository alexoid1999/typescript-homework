export type Movie = {
    id: number;
    poster_path: string;
    backdrop_path: string;
    overview: string;
    release_date: string;
    title: string;
}

export const nullMovie: Movie = {
    id: 0,
    poster_path: "string",
    backdrop_path: "string",
    overview: "string",
    release_date: "string",
    title: "string",
}

export type PageMovies = {
    page: number;
    results: Movie[];
    totalPages: number;
}