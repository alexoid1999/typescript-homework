
export type AvailableSelections = "popular" | "top_rated" | "upcoming" ;

interface IGetMovies {
    by: AvailableSelections;
    page: number;
}

export interface ISearchMovie {
    by: AvailableSelections | "search";
    filmName: string;
    page: number;
}

export type GetMovies = ISearchMovie | IGetMovies;