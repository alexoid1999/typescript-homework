import renderFavoriteList from "./components/renderFavoriteList";
import renderMoviesList from "./components/renderMoviesList";
import { AvailableSelections } from "./types/serviceType";
import clearContainer from "./services/clearContainer";

export async function render(): Promise<void> {
    // TODO render your app here
    let page = 1;
    let searchValue = "";
    let selectedSearchingMode: AvailableSelections | "search" = "popular";

    const searchInput = document.getElementById("search");

    const buttonWrapper = document.querySelectorAll('input[name="btnradio"]');
    const favouriteButton = document.getElementsByClassName("navbar-toggler")[0];
    const loadMoreButton = document.getElementById("load-more");
    const searchButton = document.getElementById("submit");

    const favoriteContainer = document.getElementById("favorite-movies")?.children[0];
    const filmContainer = document.getElementById("film-container");


    favouriteButton.addEventListener("click",()=>{
        favoriteContainer && clearContainer(favoriteContainer);
        renderFavoriteList(favoriteContainer as HTMLElement);
    })

    searchInput?.addEventListener("input", (e:Event)=>{
        e.preventDefault();
        searchValue = (e.target as HTMLInputElement).value
    });

    searchButton?.addEventListener("click", ()=>{
        selectedSearchingMode = "search";
        
        filmContainer && clearContainer(filmContainer);

        renderMoviesList(
            filmContainer, 
            {by: selectedSearchingMode, filmName: searchValue, page}
        );
    })

    buttonWrapper?.forEach(button => {
        button.addEventListener("click", async()=>{
            page = 1;
            selectedSearchingMode = button.id as AvailableSelections;

            filmContainer && clearContainer(filmContainer);

            renderMoviesList(
                filmContainer, 
                {by: selectedSearchingMode, page}
            );
        })
    });

    loadMoreButton?.addEventListener("click", async()=>{
        page += 1;
        await renderMoviesList(
            filmContainer, 
            {by: selectedSearchingMode, filmName: searchValue, page}
        );
    });

    renderMoviesList(filmContainer, {by: "popular", page});
}
